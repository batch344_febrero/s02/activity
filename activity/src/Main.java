import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        int answer = 1;
        int counter = 1;

        System.out.println("Input an integer whose factorial will be computed");
        int num = in.nextInt();

//        int i = 0;
//        while (i < num) {
//            i++;
//            answer *= i;
//        }
//        System.out.println("The factorial of " + num + " is " + answer);

        for (int x = 1; x <= num; x++) {
            counter *= x;
        }
        System.out.println("The factorial of " + num + " is " + counter);

    }
}